let trainer = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "bulbasaur"],
    friends: {
        hoen: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
    talk: function(){
        console.log("Pikachu! I choose you!");

        }
   
}
console.log(trainer);
console.log("Result of dot Notation:");
console.log(trainer.name);
console.log("Result of Square Bracket Notation");
console.log(trainer['pokemon']);
trainer.talk();

function pokemon(name, level){

    //Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        
        target.health -= this.attack
        console.log(target.name + " health is now reduceed to "+target.health);
        console.log(target);

        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted ");
    }
}

let pikachu = new pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new pokemon("Mewtwo", 100);
console.log(mewtwo);

console.log("-----------");
geodude.tackle(pikachu);
console.log("-----------");
mewtwo.tackle(geodude);
